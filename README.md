# BitChute_Mobile_Android_BottomNav
BitChute classic Android with OpenGL video encoding

/* App by:
    Ray Vahey
   && Rich Jones &&
   ```
.__                                             .___
|  |__   ____ ___  ________     ____   ____   __| _/
|  |  \_/ __ \\  \/  /\__  \   / ___\ /  _ \ / __ | 
|   Y  \  ___/ >    <  / __ \_/ /_/  >  <_> ) /_/ | 
|___|  /\___  >__/\_ \(____  /\___  / \____/\____ | 
     \/     \/      \/     \//_____/             \/ 
```
```
https://bitchute.com/channel/hexagod
https://soundcloud.com/vybemasterz

twitter @vybeypantelonez
minds @hexagod
steemit @vybemasterz
gab.ai @hexagod
```
```
based off the template by hnabbasi
https://github.com/hnabbasi/BottomNavigationViewPager
OpenGL video encoding heavily ported but courtesy of leye0
https://github.com/leye0/XamarinAndroidMediaCodecSurfaceToSurface
```
 */
